// Exercise 1
function countWage() {
  var workingDayElement = document.getElementById("workingDay");
  var workingDayValue = workingDayElement.value;
  var yourWageValue = Math.floor(+workingDayValue * 100000);
  var responseElement = document.getElementById("wage");
  responseElement.innerText = yourWageValue;
}

// Exercise 2
function calAverage() {
  var number1Element = document.getElementById("number1");
  var number1Value = number1Element.value;
  var number2Element = document.getElementById("number2");
  var number2Value = number2Element.value;
  var number3Element = document.getElementById("number3");
  var number3Value = number3Element.value;
  var number4Element = document.getElementById("number4");
  var number4Value = number4Element.value;
  var number5Element = document.getElementById("number5");
  var number5Value = number5Element.value;
  var sumAllNumber =
    +number1Value +
    +number2Value +
    +number3Value +
    +number4Value +
    +number5Value;
  var averageNumber = sumAllNumber / 5;
  var averageNumberElement = document.getElementById("average");
  averageNumberElement.innerText = averageNumber;
}

// Exercise 3
function exchangeMoney() {
  var moneyElement = document.getElementById("usdMoney");
  var moneyValue = moneyElement.value;
  var moneyExchangeValue = +moneyValue * 23500;
  var moneyExchangeElement = document.getElementById("vndMoney");
  moneyExchangeElement.innerText = moneyExchangeValue;
}

// Exercise 4
function calAreaPeri() {
  var longsElement = document.getElementById("longs");
  var longsValue = longsElement.value;
  var widthElement = document.getElementById("width");
  var widthValue = widthElement.value;
  var perimeter = (+longsValue + +widthValue) * 2;
  var perimeterElement = document.getElementById("perimeter");
  perimeterElement.innerText = perimeter;
  var area = +longsValue * +widthValue;
  var areaElement = document.getElementById("area");
  areaElement.innerText = area;
}

// Exercise 5
function calSum() {
  var numberElement = document.getElementById("number");
  var numberValue = numberElement.value;
  var unit = +numberValue % 10;
  var tens = Math.floor(+numberValue / 10);
  var sum = tens + unit;
  var sumElement = document.getElementById("sum");
  sumElement.innerText = sum;
}
